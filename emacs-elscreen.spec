%global pkg     elscreen
%global pkgname ElScreen

Name:           emacs-%{pkg}
Version:        1.4.6
Release:        1%{?dist}
Summary:        GNU Emacs window session manager

Packager:       Eugene Zamriy <eugene@zamriy.info>

Group:          Applications/Editors
License:        GPLv2+
URL:            http://www.morishima.net/~naoto/elscreen-en/
Source0:        ftp://ftp.morishima.net/pub/morishima.net/naoto/%{pkgname}/%{pkg}-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  emacs
BuildRequires:  emacs-apel

Requires:       emacs(bin) >= %{_emacs_version}
Requires:       emacs-apel

%description
%{pkgname} is a GNU Emacs window session manager modeled after GNU screen 
by Naoto Morishima.


%package        -n %{name}-el
Summary:        Elisp source files for %{pkgname} under GNU Emacs
Group:          Applications/Editors
Requires:       %{name} = %{version}-%{release}

%description    -n %{name}-el
This package contains the elisp source files for %{pkgname} under GNU Emacs. You
do not need to install this package to run %{pkgname}. Install the %{name}
package to use %{pkgname} with GNU Emacs


%prep
%setup -q -n %{pkg}-%{version}

%build
emacs --batch --eval '(setq load-path (cons "." load-path))' -f batch-byte-compile *.el

%install
%{__rm} -rf %{buildroot}
%{__install} -pm 755 -d %{buildroot}%{_emacs_sitelispdir}/%{pkg}
%{__install} -pm 644 %{_builddir}/%{pkg}-%{version}/*.elc %{buildroot}%{_emacs_sitelispdir}/%{pkg}/
%{__install} -pm 644 %{_builddir}/%{pkg}-%{version}/*.el %{buildroot}%{_emacs_sitelispdir}/%{pkg}/

%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc README ChangeLog
%{_emacs_sitelispdir}/%{pkg}/*.elc
%dir %{_emacs_sitelispdir}/%{pkg}


%files -n %{name}-el
%defattr(-, root, root, -)
%{_emacs_sitelispdir}/%{pkg}/*.el


%changelog

* Thu Feb 28 2013 Eugene Zamriy <eugene@zamriy.info> - 1.4.6-1
- initial release
